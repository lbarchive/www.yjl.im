PORT ?= 8000

all:
.PHONY: all

dev:
	python -m SimpleHTTPServer ${PORT}
.PHONY: dev
